package com.example.accounting.balance.boundary.dto

import java.math.BigDecimal
import java.math.BigDecimal.ZERO

data class BalanceDto(
    var sum: BigDecimal = ZERO,
    var id: Long? = null
)