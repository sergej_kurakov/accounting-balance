package com.example.accounting.balance.boundary.mapper

import com.example.accounting.balance.boundary.dto.BalanceDto
import com.example.accounting.balance.business.model.Balance
import org.mapstruct.Mapper

@Mapper(componentModel = "spring")
interface BalanceMapper {
    fun map(source: Balance): BalanceDto
    fun map(source: BalanceDto): Balance
}