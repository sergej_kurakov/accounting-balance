package com.example.accounting.balance.boundary.service

import com.example.accounting.balance.boundary.dto.BalanceDto
import com.example.accounting.balance.boundary.mapper.BalanceMapper
import com.example.accounting.balance.business.model.Balance
import com.example.accounting.balance.business.repo.BalanceRepo
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional
class BalanceService(private val balanceRepo: BalanceRepo, private val balanceMapper: BalanceMapper) {

    fun find() = balanceMapper.map(balanceRepo.findAll().firstOrNull() ?: Balance())

    fun save(dto: BalanceDto) {
        balanceRepo.save(balanceMapper.map(dto))
    }
}