package com.example.accounting.balance.business.model

import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Balance(
    var sum: BigDecimal = ZERO,

    @Id
    @GeneratedValue
    var id: Long? = null
)