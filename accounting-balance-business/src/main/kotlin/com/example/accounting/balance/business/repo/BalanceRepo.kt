package com.example.accounting.balance.business.repo

import com.example.accounting.balance.business.model.Balance
import org.springframework.data.repository.CrudRepository

interface BalanceRepo: CrudRepository<Balance, Long>