FROM gradle:6.0.1-jdk8 as build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon

FROM openjdk:8-jdk-alpine
RUN mkdir /app
COPY --from=build /home/gradle/src/accounting-balance-application/build/libs/*.jar /app/app.jar
ENTRYPOINT ["java","-jar","/app/app.jar"]