package com.example.accounting.balance.application.main

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.integration.annotation.IntegrationComponentScan

@SpringBootApplication
@ComponentScan(basePackages = ["com.example"])
@IntegrationComponentScan(basePackages = ["com.example"])
@EnableJpaRepositories("com.example")
@EntityScan(basePackages = ["com.example"])
class Main

fun main(args: Array<String>) {
    runApplication<Main>(*args)
}