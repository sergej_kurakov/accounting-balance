package com.example.accounting.balance.application.integration

import com.example.accounting.balance.application.dto.StatementDto
import com.example.accounting.balance.boundary.service.BalanceService
import org.springframework.stereotype.Component

@Component
class StatementHandler(private val balanceService: BalanceService) {

    fun handler(source: StatementDto) {
        println("test")
        val result = balanceService.find()
        result.sum += source.sum
        balanceService.save(result)
    }
}