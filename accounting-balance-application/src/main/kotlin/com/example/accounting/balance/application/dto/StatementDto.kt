package com.example.accounting.balance.application.dto

import java.io.Serializable
import java.math.BigDecimal
import java.math.BigDecimal.ZERO

data class StatementDto(var sum: BigDecimal = ZERO): Serializable