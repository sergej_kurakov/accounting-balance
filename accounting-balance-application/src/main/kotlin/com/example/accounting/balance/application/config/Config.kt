package com.example.accounting.balance.application.config

import com.example.accounting.balance.boundary.dto.BalanceDto
import com.example.accounting.balance.application.integration.StatementHandler
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.rabbit.core.RabbitAdmin
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.integration.amqp.dsl.Amqp
import org.springframework.integration.dsl.IntegrationFlows
import org.springframework.integration.dsl.Transformers

@Configuration
class Config {

    @Bean
    fun connectionFactory() = CachingConnectionFactory("172.25.0.2")

    @Bean
    fun rabbitAdmin(connectionFactory: ConnectionFactory) = RabbitAdmin(connectionFactory)

    @Bean
    fun rabbitTemplate(connectionFactory: ConnectionFactory) = RabbitTemplate(connectionFactory)

    @Bean
    fun statementQueue() = Queue("statementQueue")

    @Bean
    fun statementListener(connectionFactory: ConnectionFactory): SimpleMessageListenerContainer {
        val container = SimpleMessageListenerContainer()
        container.connectionFactory = connectionFactory
        container.setQueueNames("statementQueue")
        return container
    }

    @Bean
    fun statementInbound(connectionFactory: ConnectionFactory, statementHandler: StatementHandler) = IntegrationFlows
        .from(Amqp.inboundAdapter(connectionFactory, "statementQueue"))
        .transform(Transformers.fromJson(BalanceDto::class.java))
        .handle { message -> println(message.payload) }
        .get()!!
}